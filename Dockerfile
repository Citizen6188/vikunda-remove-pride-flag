# Stage 1: Build application
FROM node:18 AS compile-image

WORKDIR /build
COPY ./0001-Fuck-Pride.patch .

RUN \
  (curl -L https://kolaente.dev/vikunja/frontend/archive/main.tar.gz | tar --strip-components=1 -xvz) && \
  git apply ./0001-Fuck-Pride.patch && \
  # Build the frontend
  yarn install --frozen-lockfile --network-timeout 100000 && yarn cache clean && \
  echo '{"VERSION": "'$(git describe --tags --always --abbrev=10 | sed 's/-/+/' | sed 's/^v//' | sed 's/-g/-/')'"}' > src/version.json && \
  yarn run build

# Stage 2: copy 
FROM nginx

COPY nginx.conf /etc/nginx/nginx.conf
COPY run.sh /run.sh

# copy compiled files from stage 1
COPY --from=compile-image /build/dist /usr/share/nginx/html

# Unprivileged user
ENV PUID 1000
ENV PGID 1000

LABEL maintainer="maintainers@vikunja.io"

CMD "/run.sh"
